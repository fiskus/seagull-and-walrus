import Animation from '../animations/animation';

export default class StepLeftAnimation extends Animation {
  public getDuration () {
    return 100;
  }

  public getName () {
    return 'walrus-step-left';
  }
}
