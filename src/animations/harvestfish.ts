import Animation from '../animations/animation';

export default class HarvestFishAnimation extends Animation {
  getDuration () {
    return 3000;
  }

  public getName () {
    return 'walrus-harvest-fish';
  }
}
