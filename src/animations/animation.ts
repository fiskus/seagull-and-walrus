import Maybe from '../primitives/maybe';

abstract class Animation extends Maybe {

  private _callback: Function;
  private _isContinuing: boolean;

  constructor (callback?: Function) {
    super();

    this._isContinuing = true;

    if (callback) {
      this._callback = callback;

      setTimeout(() => {
        this._isContinuing = false;
        this._callback();
      }, this.getDuration());
    }
  }

  abstract getDuration(): number;

  abstract getName(): string;

  public isContinuing () {
    return this._isContinuing;
  }
}

export default Animation;
