import Animation from '../animations/animation';

export default class NilAnimation extends Animation {
  constructor () {
    super();

    this.setNil(true);
  }

  public getDuration (): number {
    return 0;
  }

  public getName (): string {
    return '';
  }
}
