export default {
  AMMO: {
    AMOUNT: 3,
    LIMIT: 3,
  },
  CANVAS: {
    // actual canvas, not entire world, starting from platform horizont
    HEIGHT: 600,
    WIDTH: 500,
  },
  FISH: {
    HEIGHT: 30,
    SPEED: 10,
    WIDTH: 30,
  },
  SEAGULL: {
    FALL_SPEED: 60,
    HEIGHT: 50,
    LIMIT: 30,
    OVERLAP: 30,
    RISE_SPEED: 80,
    SPEED_X: 10,
    WIDTH: 120,
  },
  WALRUS: {
    HEIGHT: 100,
    SPEED: 1,
    STEP: 20,
    WIDTH: 100,
  },
};
