import IWalrusUIData from '../interfaces/iwalrusuidata';
import Settings from '../settings/settings';
import Walrus from '../units/walrus';


export default function WalrusViewModel (walrus: Walrus): IWalrusUIData | null {
  if (!walrus.hasPosition()) {
    return null;
  }
  return {
    animation: walrus.getAnimation().getName(),
    left: getWalrusPositionX(walrus),
  };
}

function getWalrusPositionX (walrus: Walrus): number {
  if (walrus.hasVector()) {
    walrus.makeMicroStep(walrus.getVector().getDirection());
  }
  let cssOffset = walrus.getPosition().convertToCssOffset();
  return cssOffset.left - (Settings.WALRUS.WIDTH / 2);
}
