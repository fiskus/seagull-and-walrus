import IAmmoIndicatorViewData from '../interfaces/iammoindicatorviewdata';
import Walrus from '../units/walrus';


function AmmoIndicatorViewModel (walrus: Walrus): IAmmoIndicatorViewData {
  return {
    amount: walrus.getAmmoAmount(),
  };
}

export default AmmoIndicatorViewModel;
