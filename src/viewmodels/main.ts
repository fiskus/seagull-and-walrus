import AmmoIndicatorViewModel from '../viewmodels/ammoindicator';
import FishViewModel from '../viewmodels/fish';
import GameOverViewModel from '../viewmodels/gameover';
import HealthIndicatorViewModel from '../viewmodels/healthindicator';
import IMainViewData from '../interfaces/imainviewdata';
import Level from '../level';
import SeagullViewModel from '../viewmodels/seagull';
import StartLevelViewModel from '../viewmodels/startlevel';
import WalrusViewModel from '../viewmodels/walrus';


function Main (level: Level): IMainViewData {
  let walrus = level.getWalrus();
  let fishProps = FishViewModel(walrus);
  return {
    ammoIndicator: AmmoIndicatorViewModel(walrus),
    fish: fishProps,
    gameOver: GameOverViewModel(level),
    healthIndicator: HealthIndicatorViewModel(),
    seagull: SeagullViewModel(level.getSeagull(), walrus),
    startLevel: StartLevelViewModel(level),
    walrus: WalrusViewModel(walrus),
  };
}

export default Main;
