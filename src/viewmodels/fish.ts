import IFishUIData from '../interfaces/ifishuidata';
import Settings from '../settings/settings';
import Walrus from '../units/walrus';


export default function FishViewModel (walrus: Walrus): IFishUIData | null {
  if (!walrus.hasFish()) {
    return null;
  }

  let fish = walrus.getFish();
  let successfully = fish.makeMicroStep();
  if (successfully) {
    let cssOffset = fish.getPosition().convertToCssOffset();
    return {
      left: cssOffset.left + Settings.FISH.WIDTH / 2,
      top: cssOffset.top + Settings.FISH.HEIGHT / 2,
    };
  } else {
    walrus.destroyFish();
    return null;
  }
}
