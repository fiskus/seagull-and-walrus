import IStartLevelViewData from '../interfaces/istartlevelviewdata';
import Level from '../level';

function StartLevelViewModel (level: Level): IStartLevelViewData | null {
  if (!level.isNil()) {
    return null;
  }
  return {
    levels: [{
      name: '1',
      unlocked: true,
    }, {
      name: '2',
      unlocked: true,
    }],
    title: 'Select level',
  };
}

export default StartLevelViewModel;
