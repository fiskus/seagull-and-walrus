import IHealthIndicatorViewData from '../interfaces/ihealthindicatorviewdata';


export default function HealthIndicatorViewModel (): IHealthIndicatorViewData {
  return {
    amount: 3,
  };
}
