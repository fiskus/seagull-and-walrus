import IGameOverUIData from '../interfaces/igameoveruidata';
import Level from '../level';

function GameOverViewModel (level: Level): IGameOverUIData | null {
  if (!level.isGameOver()) {
    return null;
  }
  return {
    stage: level.getStage(),
    title: 'Game Over!',
  };
}

export default GameOverViewModel;
