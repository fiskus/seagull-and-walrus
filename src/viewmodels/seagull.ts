import Seagull from '../units/seagull';
import ISeagullViewData from '../interfaces/iseagullviewdata';
import Walrus from '../units/walrus';


function SeagullViewModel (seagull: Seagull, walrus: Walrus): ISeagullViewData {
  if (seagull.hasVector()) {
    let stillFlying = seagull.makeMicroStep();
    if (!stillFlying) {
      seagull.createVector();
    }
  } else {
    seagull.createVector();
  }

  let cssOffset = seagull.getPosition().convertToCssOffset();
  return {
    left: cssOffset.left,
    top: cssOffset.top,
  };
}

export default SeagullViewModel;
