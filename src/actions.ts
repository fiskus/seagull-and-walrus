import {EventEmitter} from 'events';


export let dispatcher = new EventEmitter();

export function startLevel (stage: number) {
  dispatcher.emit('startLevel', {
    stage,
  });
}
