import * as React from 'react';

export default function Platform (props: any) {
  return (
    <div className="platform">
      {props.children}
    </div>
  );
}
