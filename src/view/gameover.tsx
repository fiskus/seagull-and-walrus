import * as React from 'react';

import * as Actions from '../actions';
import IGameOverUIData from '../interfaces/igameoveruidata';


export default function GameOver (props: IGameOverUIData) {
  return (
    <div className="game-over">
      <h1 className="game-over-title">{props.title}</h1>
      <div className="game-over-content">
        <button onClick={() => Actions.startLevel(props.stage)}>Retry</button>
      </div>
    </div>
  );
}
