import * as React from 'react';

import HealthIndicatorItem from '../view/healthindicatoritem';
import IHealthIndicatorViewData from '../interfaces/ihealthindicatorviewdata';


function repeat (times: number, func: Function) {
  for (let i = 0; i < times; i++) {
    func(i);
  }
}

export default function HealthIndicator (props: IHealthIndicatorViewData) {
  return (
    <div className="indicator-health">
      {repeat(props.amount, () => {
         <HealthIndicatorItem />
       })}
    </div>
  );
}
