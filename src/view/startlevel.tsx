import * as React from 'react';

import IStartLevelViewData from '../interfaces/istartlevelviewdata';
import StartLevelItem from '../view/startlevelitem';


export default function StartLevel (props: IStartLevelViewData) {
  return (
    <div className="start-level">
      <h1 className="start-level-title">
        {props.title}
      </h1>
      <div className="start-level-content">
        {props.levels.map((levelData, index) => {
          return <StartLevelItem {...levelData} key={index} />;
        })}
      </div>
    </div>
  );
}
