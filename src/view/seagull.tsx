import * as React from 'react';

import ISeagullViewData from '../interfaces/iseagullviewdata';


export default function Seagull (props: ISeagullViewData) {
  let styles: React.CSSProperties = {
    transform: `translate(${props.left}px, ${props.top}px)`
  };
  return (
    <div className="seagull" style={styles} />
  );
}
