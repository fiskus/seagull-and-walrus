import * as React from 'react';

import AmmoIndicatorItem from '../view/ammoindicatoritem';
import IAmmoIndicatorViewData from '../interfaces/iammoindicatorviewdata';


function repeat (times: number, func: Function): Array<any> {
  let output: Array<any> = [];
  for (let i = 0; i < times; i++) {
    output.push(func(i));
  }
  return output;
}

export default function AmmoIndicator (props: IAmmoIndicatorViewData) {
  return (
    <div className="indicator-ammo">
      {repeat(props.amount, (index: number) => {
        return <AmmoIndicatorItem key={index} />;
      })}
    </div>
  );
}

