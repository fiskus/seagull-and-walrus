import * as React from 'react';

import IFishUIData from '../interfaces/ifishuidata';
import Nil from '../view/nil';


export default function Fish (props: IFishUIData) {
  if (!props) {
    return <Nil />;
  }
  let styles: React.CSSProperties = {
    transform: `translate(${props.left}px, ${props.top}px)`
  };
  return (
    <div className="fish" style={styles} />
  );
}
