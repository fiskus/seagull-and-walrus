import * as React from 'react';

import AmmoIndicator from '../view/ammoindicator';
import Fish from '../view/fish';
import GameOver from '../view/gameover';
import HealthIndicator from '../view/healthindicator';
import IMainViewData from '../interfaces/imainviewdata';
import Nil from '../view/nil';
import Platform from '../view/platform';
import Seagull from '../view/seagull';
import StartLevel from '../view/startlevel';
import Walrus from '../view/walrus';
import World from '../view/world';


export default function Main (props: IMainViewData) {
  return (
    <World {...props}>
      {props.startLevel ? <StartLevel {...props.startLevel} /> : <Nil />}
      <Seagull {...props.seagull} />
      <HealthIndicator {...props.healthIndicator} />
      <AmmoIndicator {...props.ammoIndicator} />
      {props.fish ? <Fish {...props.fish} /> : <Nil />}
      <Platform {...props}>
        {props.walrus ? <Walrus {...props.walrus} /> : <Nil />}
      </Platform>
      {props.gameOver ? <GameOver {...props.gameOver} /> : <Nil />}
    </World>
  );
}
