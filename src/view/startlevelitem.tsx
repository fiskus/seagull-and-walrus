import * as React from 'react';

import * as Actions from '../actions';
import IStartLevelItemViewData from '../interfaces/istartlevelitemviewdata';

export default function StartLevelItem (props: IStartLevelItemViewData) {
  return (
    <div className="start-level-item"
         onClick={() => Actions.startLevel(Number(props.name))}>
      {props.name}
    </div>
  );
}
