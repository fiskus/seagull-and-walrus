import * as React from 'react';


export default function World (props: any) {
  return (
    <div className="world">
      {props.children}
    </div>
  );
}
