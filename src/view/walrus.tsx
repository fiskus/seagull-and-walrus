import * as React from 'react';

import IWalrusUIData from '../interfaces/iwalrusuidata';


export default function Walrus (props: IWalrusUIData) {
  let className: string = ['walrus', props.animation].join(' ');
  let styles: React.CSSProperties = {
    transform: `translateX(${props.left}px)`
  };
  return (
    <div className={className} style={styles} />
  );
}
