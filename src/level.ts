import Direction from './primitives/arrowtemp';
import Maybe from './primitives/maybe';
import Platform from './units/platform';
import Position from './primitives/position';
import Seagull from './units/seagull';
import Settings from './settings/settings';
import Walrus from './units/walrus';


const keys = {
  DOWN: 'ArrowDown',
  LEFT: 'ArrowLeft',
  RIGHT: 'ArrowRight',
  UP: 'ArrowUp',
};

export default class Level extends Maybe {
  static nil (): Level {
    let stage: number = 0;
    return new Level(stage);
  }

  static start (stage: number): Level {
    return new Level(stage);
  }

  private _isGameOver: boolean;
  private _platform: Platform;
  private _seagull: Seagull;
  private _stage: number;
  private _walrus: Walrus;

  constructor (stage: number) {
    super();

    this.setNil(stage === 0);

    this._stage = stage;

    this._isGameOver = false;
    this._platform = new Platform();
    this._seagull = this.seagullFactory();
    this._walrus = this.walrusFactory();

    this.listenKeys(); // TODO: create keyboard listener class
  }

  public getWalrus (): Walrus {
    return this._walrus;
  }

  public getSeagull (): Seagull {
    return this._seagull;
  }

  public getStage (): number {
    return this._stage;
  }

  public isGameOver (): boolean {
    return this._isGameOver;
  }

  public stepLoop () {
    if (this._walrus.hasFish()) {
      if (this.doesFishHitSeagull()) {
        this._seagull.feed();
        this._walrus.destroyFish();
      }
    }

    if (this.doesSeagullHitWalrus()) {
      this._isGameOver = true;
    }
  }

  private seagullFactory () {
    let seagull = new Seagull();
    let seagullStartPosition = Position.parse(
        Settings.CANVAS.WIDTH / 2, Settings.CANVAS.HEIGHT);
    seagull.setPosition(seagullStartPosition);
    seagull.setSpeedX(Settings.SEAGULL.SPEED_X * this.getStage());
    return seagull;
  }

  private walrusFactory () {
    let walrus = new Walrus();
    let startPosition = Position.parse(0, 0);
    walrus.setPosition(startPosition);
    walrus.setSpeed(Settings.WALRUS.SPEED);
    return walrus;
  }

  private listenKeys () {
    document.addEventListener('keyup', (event) => {
      switch (event.key) {
        case keys.UP:
          this.onKeyUp();
          break;
        case keys.DOWN:
          this.onKeyDown();
          break;
        case keys.LEFT:
          this.onKeyLeft();
          break;
        case keys.RIGHT:
          this.onKeyRight();
          break;
      }
    });
  }

  private onKeyUp () {
    this._walrus.useFish();
  }

  private onKeyDown () {
    this._walrus.harvestFish();
  }

  private onKeyLeft () {
    this._walrus.makeStep(Direction.left(), this._platform);
  }

  private onKeyRight () {
    this._walrus.makeStep(Direction.right(), this._platform);
  }

  private doesFishHitSeagull () {
    let xOverlap = (Settings.SEAGULL.WIDTH / 2) + (Settings.FISH.WIDTH / 2);
    let yOverlap = (Settings.SEAGULL.HEIGHT / 2) + (Settings.FISH.HEIGHT / 2);
    return this._walrus.getFish().getPosition().isOverlap(
        this._seagull.getPosition(), xOverlap, yOverlap);
  }

  private doesSeagullHitWalrus () {
    if (!this._walrus.hasPosition() || !this._walrus.hasPosition()) {
      return false;
    }

    let xOverlap = (Settings.SEAGULL.WIDTH / 2) + (Settings.WALRUS.WIDTH / 2);
    let yOverlap = (Settings.SEAGULL.HEIGHT / 2) + (Settings.WALRUS.HEIGHT / 2);
    return this._walrus.getPosition().isOverlap(
        this._seagull.getPosition(), xOverlap, yOverlap);
  }
}
