interface IStartLevelItemViewData {
  name: string;
  unlocked: boolean;
}

export default IStartLevelItemViewData;
