interface IGameOverUIData {
  stage: number;
  title: string;
}

export default IGameOverUIData;
