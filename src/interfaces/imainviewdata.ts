import IAmmoIndicatorViewData from '../interfaces/iammoindicatorviewdata';
import IFishUIData from '../interfaces/ifishuidata';
import IGameOverUIData from '../interfaces/igameoveruidata';
import IHealthIndicatorViewData from '../interfaces/ihealthindicatorviewdata';
import ISeagullViewData from '../interfaces/iseagullviewdata';
import IStartLevelViewData from '../interfaces/istartlevelviewdata';
import IWalrusUIData from '../interfaces/iwalrusuidata';


export interface IMainViewData {
  ammoIndicator: IAmmoIndicatorViewData;
  fish: IFishUIData | null;
  gameOver: IGameOverUIData | null;
  healthIndicator: IHealthIndicatorViewData;
  seagull: ISeagullViewData;
  startLevel: IStartLevelViewData | null;
  walrus: IWalrusUIData | null;
}

export default IMainViewData;
