interface IWalrusUIData {
  left: number;
  animation: string;
}

export default IWalrusUIData;
