interface ISeagullViewData {
  left: number;
  top: number;
}

export default ISeagullViewData;
