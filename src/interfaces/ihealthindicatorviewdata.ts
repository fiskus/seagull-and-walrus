interface IHealthIndicatorViewData {
  amount: number;
}

export default IHealthIndicatorViewData;
