import IStartLevelItemViewData from '../interfaces/istartlevelitemviewdata';

interface IStartLevelViewData {
  levels: IStartLevelItemViewData[];
  title: string;
}

export default IStartLevelViewData;
