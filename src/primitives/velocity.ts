import Maybe from '../primitives/maybe';
import Speed from '../primitives/speed';


export default class Velocity extends Maybe {
  static nil () {
    return new Velocity(Speed.nil(), Speed.nil());
  }

  static parse (x: number, y: number) {
    return new Velocity(new Speed(x), new Speed(y));
  }

  private _x: Speed;
  private _y: Speed;

  constructor (x: Speed, y: Speed) {
    super();

    this.setFulfilled(x.isFulfilled() && y.isFulfilled());

    this._x = x;
    this._y = y;
  }

  public getX (): Speed {
    this.assertFullfillness();

    return this._x;
  }

  public getY (): Speed {
    this.assertFullfillness();

    return this._y;
  }
}
