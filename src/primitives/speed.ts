import Maybe from '../primitives/maybe';


export default class Speed extends Maybe {
  static nil () {
    return new Speed();
  }

  static parse (value: number) {
    return new Speed(value);
  }

  private _value: number;

  constructor (value?: number) {
    super();

    this.setFulfilled(typeof value === 'number');

    this._value = value || 0;
  }

  public getValue (): number {
    this.assertFullfillness();

    return this._value;
  }
}
