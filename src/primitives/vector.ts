import Direction from '../primitives/arrowtemp';
import Maybe from '../primitives/maybe';
import Position from '../primitives/position';
import Velocity from '../primitives/velocity';


export default class Vector extends Maybe {
  static nil () {
    return new Vector(Position.nil(), Position.nil(), Velocity.nil());
  }

  private _start: Position;
  private _end: Position;
  private _velocity: Velocity;

  constructor (start: Position, end: Position, velocity: Velocity) {
    super();

    this.setFulfilled(start.isFulfilled() && end.isFulfilled());

    this._start = start;
    this._end = end;
    this._velocity = velocity;
  }

  public getStart (): Position {
    return this._start;
  }

  public getEnd (): Position {
    return this._end;
  }

  public getDirection (): Direction {
    this.assertFullfillness();

    return Direction.parse(this);
  }

  public getVelocity (): Velocity {
    return this._velocity;
  }

  public convertToXBounds (): number[] {
    this.assertFullfillness();

    return [this._start.getX().getValue(), this._end.getX().getValue()];
  }

  public convertToYBounds (): number[] {
    this.assertFullfillness();

    return [this._start.getY().getValue(), this._end.getY().getValue()];
  }

  public contains (position: Position): boolean {
    this.assertFullfillness();

    return position.getX().isInsideBounds(this.convertToXBounds()) &&
        position.getY().isInsideBounds(this.convertToYBounds());
  }
}
