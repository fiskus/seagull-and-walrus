export default class Maybe {
  private _isNil: boolean;

  constructor () {
    this._isNil = false;
  }

  public isNil (): boolean {
    return this._isNil;
  }

  public isFulfilled (): boolean {
    return !this._isNil;
  }

  public setNil (isNil: boolean) {
    this._isNil = isNil;
  }

  public setFulfilled (isFulfilled: boolean) {
    this._isNil = !isFulfilled;
  }

  public assertFullfillness () {
    if (this._isNil) {
      throw new Error();
    }
  }
}
