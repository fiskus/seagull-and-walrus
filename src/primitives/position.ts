import Coordinate from '../primitives/coordinate';
import Direction from '../primitives/arrowtemp';
import Maybe from '../primitives/maybe';
import Settings from '../settings/settings';
import Velocity from '../primitives/velocity';


export default class Position extends Maybe {
  static nil (): Position {
    return new Position(Coordinate.nil(), Coordinate.nil());
  }

  static parse (x: number, y: number): Position {
    return new Position(new Coordinate(x), new Coordinate(y));
  }

  private _x: Coordinate;
  private _y: Coordinate;

  constructor (x: Coordinate, y: Coordinate) {
    super();

    this.setFulfilled(x.isFulfilled() && y.isFulfilled());

    this._x = x;
    this._y = y;
  }

  public getX (): Coordinate {
    this.assertFullfillness();

    return this._x;
  }

  public getY (): Coordinate {
    this.assertFullfillness();

    return this._y;
  }

  public clone (): Position {
    this.assertFullfillness();

    return Position.parse(this._x.getValue(), this._y.getValue());
  }

  public shiftBy (direction: Direction, velocity: Velocity): Position {
    this.assertFullfillness();

    let destinationX = this._x.clone();
    destinationX.shiftBy(
        direction.getHorizontalSign() * velocity.getX().getValue());

    let destinationY = this._y.clone();
    destinationY.shiftBy(
        direction.getVerticalSign() * velocity.getY().getValue());
    return new Position(destinationX, destinationY);
  }

  public convertToCssOffset (): {left: number, top: number} {
    this.assertFullfillness();

    return {
      left: this._x.getValue() + (Settings.CANVAS.WIDTH / 2),
      top: Settings.CANVAS.HEIGHT - this._y.getValue(),
    };
  }

  public isOverlap (
      position: Position, xOverlap: number, yOverlap: number): boolean {
    this.assertFullfillness();

    let xDiff = Math.abs(this._x.getValue() - position.getX().getValue());
    let yDiff = Math.abs(this._y.getValue() - position.getY().getValue());
    return xDiff < xOverlap && yDiff < yOverlap;
  }
}
