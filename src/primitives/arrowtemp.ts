import Maybe from '../primitives/maybe';
import Vector from '../primitives/vector';


enum Horizontal {
  LEFT,
  NONE,
  RIGHT,
}

enum Vertical {
  DOWN,
  NONE,
  UP,
}

export default class Direction extends Maybe {
  static nil () {
    return new Direction(Horizontal.NONE, Vertical.NONE);
  }

  static parse (vector: Vector) {
    let start = vector.getStart();
    let end = vector.getEnd();
    let horizontal = end.getX().getValue() > start.getX().getValue() ?
        Horizontal.RIGHT : Horizontal.LEFT;
    let vertical = end.getY().getValue() > start.getY().getValue() ?
        Vertical.UP : Vertical.DOWN;
    return new Direction(horizontal, vertical);
  }

  static left () {
    return new Direction(Horizontal.LEFT, Vertical.NONE);
  }

  static right () {
    return new Direction(Horizontal.RIGHT, Vertical.NONE);
  }

  static up () {
    return new Direction(Horizontal.NONE, Vertical.UP);
  }

  static down () {
    return new Direction(Horizontal.NONE, Vertical.DOWN);
  }

  private _x: Horizontal;
  private _y: Vertical;

  constructor (x: Horizontal, y: Vertical) {
    super();

    this.setNil(x === Horizontal.NONE && y === Vertical.NONE);

    this._x = x;
    this._y = y;
  }

  getHorizontal (): Horizontal {
    return this._x;
  }

  getVertical (): Vertical {
    return this._y;
  }

  getHorizontalSign (): number {
    switch (this._x) {
      case Horizontal.LEFT:
        return -1;
      case Horizontal.RIGHT:
        return 1;
      default:
        return 1;
    }
  }

  getVerticalSign (): number {
    switch (this._y) {
      case Vertical.DOWN:
        return -1;
      case Vertical.UP:
        return 1;
      default:
        return 1;
    }
  }
}
