import Maybe from '../primitives/maybe';


export default class Coordinate extends Maybe {
  static nil (): Coordinate {
    return new Coordinate();
  }

  private _value: number;

  constructor (value?: number) {
    super();

    this.setFulfilled(typeof value === 'number');

    this._value = value || 0;
  }

  public getValue (): number {
    this.assertFullfillness();

    return this._value;
  }

  public clone () {
    this.assertFullfillness();

    return new Coordinate(this._value);
  }

  public shiftBy (value: number) {
    this.assertFullfillness();

    this._value = this._value + value;
  }

  public isInsideBounds (bounds: number[]): boolean {
    this.assertFullfillness();

    let [start, end] = bounds[0] > bounds[1] ?
        [bounds[1], [bounds[0]]] : bounds;
    return this._value >= start && this._value <= end;
  }
}
