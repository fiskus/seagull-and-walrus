import Settings from '../settings/settings';


export default class Coordinate {
  private _leftLimit: number;
  private _rightLimit: number;

  constructor () {
    this._leftLimit = - Settings.CANVAS.WIDTH / 2 + Settings.WALRUS.WIDTH / 2;
    this._rightLimit = Settings.CANVAS.WIDTH / 2 - Settings.WALRUS.WIDTH / 2;
  }

  public getBounds (): number[] {
    return [this._leftLimit, this._rightLimit];
  }
}
