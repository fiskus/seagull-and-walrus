import Direction from '../primitives/arrowtemp';
import Maybe from '../primitives/maybe';
import Position from '../primitives/position';
import Vector from '../primitives/vector';


export default class Fish extends Maybe {
  static nil (): Fish {
    return new Fish(Vector.nil());
  }

  private _position: Position;
  private _vector: Vector;

  constructor (vector: Vector) {
    super();

    this.setFulfilled(vector.isFulfilled());

    this._vector = vector;
    this._position = vector.getStart();
  }

  public getPosition (): Position {
    return this._position;
  }

  public setPosition (position: Position) {
    this._position = position;
  }

  public getVector (): Vector {
    return this._vector;
  }

  public makeMicroStep (): boolean {
    let destination = this._position.shiftBy(
        Direction.parse(this._vector), this._vector.getVelocity());

    // TODO: vector contains destination?
    if (destination.getY().isInsideBounds(this._vector.convertToYBounds())) {
      this.setPosition(destination);
      return true;
    }
    return false;
  }
}
