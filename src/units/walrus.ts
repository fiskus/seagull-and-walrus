import Animation from '../animations/animation';
import Direction from '../primitives/arrowtemp';
import Fish from '../units/fish';
import HarvestFishAnimation from '../animations/harvestfish';
import NilAnimation from '../animations/nil';
import Platform from '../units/platform';
import Position from '../primitives/position';
import Settings from '../settings/settings';
import Vector from '../primitives/vector';
import Velocity from '../primitives/velocity';


export default class Walrus {
  private _animation: Animation;
  private _fish: Fish;
  private _position: Position;
  private _speed: number;
  private _vector: Vector;
  private _ammo: number;

  constructor () {
    this._ammo = Settings.AMMO.AMOUNT;
    this._animation = new NilAnimation();
    this._fish = Fish.nil();
    this._position = Position.nil();
    this._speed = 0;
    this._vector = Vector.nil();
  }

  public setVector (vector: Vector) {
    this._vector = vector;
  }

  public hasVector (): boolean {
    return this._vector.isFulfilled();
  }

  public getVector (): Vector {
    return this._vector;
  }

  public setPosition (position: Position) {
    this._position = position;
  }

  public getPosition (): Position {
    return this._position;
  }

  public hasPosition (): boolean {
    return this._position.isFulfilled();
  }

  public getAnimation () {
    return this._animation;
  }

  public hasAnimation (): boolean {
    return this._animation.isFulfilled();
  }

  public getFish (): Fish {
    return this._fish;
  }

  public hasFish (): boolean {
    return this._fish.isFulfilled();
  }

  public setSpeed (speed: number) {
    this._speed = speed;
  }

  public getSpeed (): number {
    return this._speed;
  }

  public useFish () {
    if (this.hasAnimation() || this.hasFish() || this._ammo === 0) {
      return;
    }
    let destination = this._position.shiftBy(
        Direction.up(), Velocity.parse(0, Settings.CANVAS.HEIGHT));
    let speed = Settings.FISH.SPEED;
    let vector = new Vector(
      this._position, destination, Velocity.parse(0, speed));

    this._fish = new Fish(vector);

    this._ammo--;
  }

  public destroyFish () {
    this._fish = Fish.nil();
  }

  public harvestFish () {
    if (this.hasAnimation()) {
      return;
    }
    if (this._ammo < Settings.AMMO.LIMIT) {
      this.executeAnimation(new HarvestFishAnimation(() => {
        this._animation = new NilAnimation();
        this._ammo++;
      }));
    }
  }

  public getAmmoAmount () {
    return this._ammo;
  }

  public makeStep (direction: Direction, platform: Platform): boolean {
    if (this._animation.isFulfilled()) {
      return false;
    }
    let bounds = platform.getBounds();
    let destination = this._position.shiftBy(
        direction, Velocity.parse(Settings.WALRUS.STEP, 0));
    if (destination.getX().isInsideBounds(bounds)) {
      this._vector = new Vector(
          this._position, destination, Velocity.parse(this._speed, 0));
      return true;
    }
    return false;
  }

  public makeMicroStep (direction: Direction): boolean {
    let destination = this._position.shiftBy(
        this._vector.getDirection(), this._vector.getVelocity());
    // TODO: vector contains position
    if (destination.getX().isInsideBounds(this._vector.convertToXBounds())) {
      this.setPosition(destination);
      return true;
    }
    return false;
  }

  private executeAnimation (animation: Animation) {
    this._animation = animation;
  }
}
