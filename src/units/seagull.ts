import {ceil, random} from 'lodash';

import Direction from '../primitives/arrowtemp';
import Position from '../primitives/position';
import Settings from '../settings/settings';
import Vector from '../primitives/vector';
import Velocity from '../primitives/velocity';


export default class Seagul {
  private _fallPasses: number;
  private _fallTimer: number;
  private _position: Position;
  private _speedX: number;
  private _speedY: number;
  private _vector: Vector;

  constructor () {
    this._fallPasses = 0;
    this._fallTimer = 0;
    this._position = Position.nil();
    this._speedX = 0;
    this._speedY = 0;
    this._vector = Vector.nil();
  }

  public setVector (vector: Vector) {
    this._vector = vector;
  }

  public hasVector (): boolean {
    return this._vector.isFulfilled();
  }

  public getVector (): Vector | null {
    return this._vector;
  }

  public setPosition (position: Position) {
    this._position = position;
  }

  public getPosition (): Position {
    return this._position;
  }

  public hasPosition (): boolean {
    return this._position.isFulfilled();
  }

  public setSpeedX (speedX: number) {
    this._speedX = speedX;
  }

  public setSpeedY (speedY: number) {
    this._speedY = speedY;
  }

  public getSpeedX (): number {
    return this._speedX;
  }

  public getSpeedY (): number {
    return this._speedY;
  }

  public createVector () {
    let randomX = random(
        (Settings.CANVAS.WIDTH / 2) - Settings.SEAGULL.LIMIT,
        Settings.CANVAS.WIDTH / 2);
    let directionSign = this._position.getX().getValue() >= 0 ? -1 : 1;
    let endX = directionSign * randomX;
    let endY = this._position.getY().getValue() - Settings.SEAGULL.FALL_SPEED;

    let steps = (endX - this._position.getX().getValue()) / this._speedX;
    let speedY = (endY - this._position.getY().getValue()) / steps;
    this._vector = new Vector(
        this._position,
        Position.parse(endX, endY),
        Velocity.parse(this._speedX, speedY));

    if (speedY < 1) {
      this.setSpeedY(0);
      this._fallTimer = 0;
      this._fallPasses = ceil(Math.abs(1 / speedY));
    }
  }

  public makeMicroStep () {
    if (!this.hasPosition()) {
      return false;
    }
    let destination = this.createMicroStepDestination();
    if (this._vector.contains(destination)) {
      this.setPosition(destination);
      return true;
    }
    return false;
  }

  public feed () {
    let destination = this._position.shiftBy(
        Direction.up(), Velocity.parse(0, Settings.SEAGULL.RISE_SPEED));
    this.setPosition(destination);
    this.createVector();
  }

  private createMicroStepSpeedY () {
    if (this._speedY) {
      return this._speedY;
    } else {
      if (this._fallTimer === this._fallPasses) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  private createMicroStepDestination () {
    let speedY = this.createMicroStepSpeedY();

    if (this._fallTimer === this._fallPasses) {
      this._fallTimer = 0;
    } else {
      this._fallTimer++;
    }

    return this._position.shiftBy(
        this._vector.getDirection(), Velocity.parse(this._speedX, speedY));
  }
}
