import * as React from 'react';
import * as ReactDOM from 'react-dom';

import * as Actions from './actions';
import IMainViewData from './interfaces/imainviewdata';
import Level from './level';
import MainUI from './view/main';
import MainViewModel from './viewmodels/main';


let rootElement = document.getElementById('app');

let level = Level.nil();

function main () {
  // render(MainViewModel(Level.nil()));
  render(MainViewModel(level));
  Actions.dispatcher.addListener('startLevel', onStartLevel);
}

function onStartLevel (data: {stage: number}) {
  level = Level.start(data.stage);
  window.requestAnimationFrame(onUILoop);
}

function onUILoop () {
  render(MainViewModel(level));
  level.stepLoop();

  if (level.isGameOver()) {
    render(MainViewModel(level));
  } else {
    window.requestAnimationFrame(onUILoop);
  }
}

function render (state: IMainViewData) {
  ReactDOM.render(React.createElement(MainUI, state), rootElement);
}

main();
